package org.ffwd.gamebook.web;

import junit.framework.TestCase;

public class WebConnectionTest extends TestCase implements WebConnectionI {

	public void testFormatter() {
		assertEquals("002", String.format("%03d", 2));
	}

	@Override
	public String getIntroPage() {
		return null;
	}

	public void testIntroPage() {
		WebConnectionI wci = new WebConnection(null, null, null);
		String response = wci.getIntroPage();
		assertEquals(
				"Quello che stai per leggere non è un normale libro.\nScoprirai nelle pagine che seguiranno come dare forma al tuo\npersonaggio e come agire nel corso dell\u2019avventura.\nPerché il racconto abbia un inizio, uno svolgimento e\nuna fine dovrai seguire le indicazioni che ti verranno\ndate alla fine di ogni paragrafo.\nSarai tu a decidere le sorti del personaggio che andrai a\nincarnare.\n",
				response);
	}

	@Override
	public String getPage(String pageName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStartedOn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPlayer() {
		return null;
	}

	public void testPlayer() {
		WebConnectionI wci = new WebConnection(null, null, null);
		String response = wci.getPlayer();
		assertTrue(response.startsWith("[\"noname\","));
	}

	@Override
	public String getFight(String pageName) {
		// TODO Auto-generated method stub
		return null;
	}

	public void testFight() {
		WebConnectionI wci = new WebConnection(null, null, null);
		String response = wci.getFight("28");
		assertNotNull(response);
		response = wci.getFight("1");
		assertEquals("[]", response);
	}

	@Override
	public String getInventary() {
		return null;
	}

	public void testInventaryPostIntroPage() {
		WebConnectionI wci = new WebConnection(null, null, null);
		wci.getIntroPage();
		String response = wci.getInventary();
		assertEquals("[\"torcia\",1,\"spada\",1000]", response);
	}

	@Override
	public String getUse(String oggetto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInUse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGive(String oggetto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTake(String oggetto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLearn(String info) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTell(String info) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRestart(String info) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBookmarks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBookmark(String bookmardId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBookmarkSaved() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBookmarkRemoved(String bookmardId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTalks(String pageName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSprites(String pageName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBooks() {
		// TODO Auto-generated method stub
		return null;
	}

}
