package org.ffwd.gamebook;

import java.util.Map;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Undefined;

public class EvalRhino implements IEval {

	@Override
	public Object eval(String script) throws Exception {
		Context cx = Context.enter();
		Scriptable scope = cx.initStandardObjects();
		Object result = null;
		try {
			result = cx.evaluateString(scope, script, "<cmd>", 1, null);
		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public Object eval(String script, Map<String, Object> arguments)
			throws Exception {
		Context cx = Context.enter();
		Scriptable scope = cx.initStandardObjects();
		for (Map.Entry<String, Object> argument : arguments.entrySet()) {
			scope.put(argument.getKey(), scope, argument.getValue());
			// engine.put(argument.getKey(), argument.getValue());
		}
		Object result = null;
		try {
			result = cx.evaluateString(scope, script, "<cmd>", 1, null);
			if (result.equals(Undefined.instance))
				result = "";
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
}
