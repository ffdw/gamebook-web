package org.ffwd.gamebook.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ffwd.gamebook.Battle;
import org.ffwd.gamebook.Book;
import org.ffwd.gamebook.Bookmark;
import org.ffwd.gamebook.GameBook;
import org.ffwd.gamebook.GameBookInfo;
import org.ffwd.gamebook.Oggetto;
import org.ffwd.gamebook.Page;
import org.ffwd.gamebook.Player;
import org.json.JSONArray;

public class WebConnection implements WebConnectionI {
	final String COOKIE_NAME = "gamebook";

	protected Date startDate;
	protected GameBook gb;
	protected List<GameBookInfo> gameBookInfos;
	protected ServletContext context;
	protected HttpServletRequest request;
	protected HttpServletResponse response;

	public WebConnection(ServletContext context, HttpServletRequest request,
			HttpServletResponse response) {
		this.startDate = new Date();
		this.context = context;
		this.request = request;
		this.response = response;

		gameBookInfos = new ArrayList<GameBookInfo>();

		GameBookInfo gbi = new GameBookInfo();
		gbi.setAuthor("Nicola Pedot");
		gbi.setTitle("Specchio");
		gbi.setSummary("Salva la terra di Sirdania dallo spietato Mano Nera");
		gbi.setCollection("Soul Key");
		gbi.setPublishedOn(new Date());
		gbi.setNrPages(400);
		gameBookInfos.add(gbi);

		getRestart(gbi.getTitle().toLowerCase() + ".txt");
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	@Override
	public String getRestart(String gameName) {
		this.gb = new GameBook();
		WebBookImporter bookImporter = new WebBookImporter();
		String bookName = gameName;
		Book book = bookImporter.read(context, bookName);
		gb.setBook(book);
		System.out.println("BOOK TITLE:" + book.getTitle() + " ("
				+ book.getNrPages() + " pages)");
		Page introPage = book.getPage("INTRO");
		System.out.println("BOOK INTRO:" + introPage);
		return "['OK']";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ffwd.gamebook.web.WebConnectionI#getIntroPage()
	 */
	@Override
	public String getIntroPage() {
		Page introPage = gb.getBook().getPage("INTRO");
		String name = "torcia";
		int dispo = 1;
		int mana = 0;
		int abilita = 0;
		int stamina = 0;
		// TODO Creazione personaggio.
		gb.getP().addOggetto(new Oggetto(name, dispo, mana, abilita, stamina));
		Oggetto o = new Oggetto("spada", 1000, 5, 5, 0);
		o.setAttacco(5);
		o.setDifesa(2);
		gb.getP().addOggetto(o);

		return introPage.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ffwd.gamebook.web.WebConnectionI#getPage(java.lang.String)
	 */
	@Override
	public String getPage(String pageName) {
		JSONArray arrayObj = new JSONArray();
		List<String> arrayStr = gb.playPage(pageName);
		for (String string : arrayStr) {
			arrayObj.put(string);
		}
		return arrayObj.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ffwd.gamebook.web.WebConnectionI#getStartedOn()
	 */
	@Override
	public String getStartedOn() {
		return startDate.toString();
	}

	public Date getStartDate() {
		return startDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ffwd.gamebook.web.WebConnectionI#getPlayer()
	 */
	@Override
	public String getPlayer() {
		Player p = gb.getP();
		JSONArray arrayObj = new JSONArray();
		arrayObj.put(p.getNome());
		arrayObj.put(p.getAbilita());
		arrayObj.put(p.getStamina());
		arrayObj.put(p.getMana());
		arrayObj.put(p.getExperience());
		arrayObj.put(p.getGilda());
		return arrayObj.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ffwd.gamebook.web.WebConnectionI#getFight(java.lang.String)
	 */
	@Override
	public String getFight(String pageName) {
		Page page = gb.getPage(pageName);
		Battle battle = new Battle();// gb.getBattle(page);
		battle.setNotPlayer(page.getNp().duplicate());
		battle.setPlayer(gb.getP());
		String esito = battle.round();
		JSONArray arrayObj = new JSONArray();
		arrayObj.put(esito);
		return arrayObj.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ffwd.gamebook.web.WebConnectionI#getInventary()
	 */
	@Override
	public String getInventary() {
		List<Oggetto> inventario = gb.getP().getInventario();
		JSONArray arrayObj = new JSONArray();
		for (Oggetto oggetto : inventario) {
			arrayObj.put(oggetto.getName());
			arrayObj.put(oggetto.getDisponibilita());
		}
		return arrayObj.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ffwd.gamebook.web.WebConnectionI#getUse(String oggetto)
	 */
	@Override
	public String getUse(String oggetto) {
		Player p = gb.getP();
		Oggetto inUso = null;
		if ("-".equals(oggetto)) {
			p.setInUso(null);
		} else {
			for (Oggetto o : p.getInventario()) {
				if (o.getName().equals(oggetto)) {
					p.setInUso(o);
					inUso = o;
				}
			}
		}
		String esito = (inUso != null) ? inUso.toString() : "";
		JSONArray arrayObj = new JSONArray();
		arrayObj.put(esito);
		return arrayObj.toString();
	}

	@Override
	public String getInUse() {
		JSONArray arrayObj = new JSONArray();
		Player p = gb.getP();
		String inUse = (p.getInUso() != null) ? p.getInUso().toString() : null;
		if (inUse != null)
			arrayObj.put(inUse);
		return arrayObj.toString();
	}

	@Override
	public String getGive(String oggetto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTake(String oggetto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLearn(String info) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTell(String info) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Ritorna elenco dei bookmark in gamebook.
	 */
	@Override
	public String getBookmarks() {

		List<Bookmark> bookmarks = new ArrayList<Bookmark>();

		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cook : cookies) {
				if (cook.getName().startsWith(COOKIE_NAME)) {
					System.out.println(cook.getValue());
					if (cook.getMaxAge() != 0) {
						bookmarks
								.add(new Bookmark().setString(cook.getValue()));
					}
				}
			}
		}

		JSONArray arrayObj = new JSONArray();
		for (Bookmark bookmark : bookmarks) {
			arrayObj.put(bookmark.getCurrentPageKey());
		}
		return arrayObj.toString();
	}

	/**
	 * Restore the bookmark with 'bookmarkid' and return the new current page
	 * key.
	 */
	@Override
	public String getBookmark(String bookmarkId) {
		JSONArray arrayObj = new JSONArray();
		String pageKey;
		String bookmarkTxt = getCookie(bookmarkId);
		if (bookmarkTxt != null) {
			pageKey = gb
					.restoreBookmark((new Bookmark().setString(bookmarkTxt)));
		} else {

			pageKey = gb.restoreBookmark(new Integer(bookmarkId));
		}
		arrayObj.put(pageKey);
		return arrayObj.toString();
	}

	/**
	 * Return named cookie;
	 * 
	 * @param bookmarkId
	 * @return
	 */
	private String getCookie(String bookmarkId) {
		Cookie[] cookies = request.getCookies();
		Cookie cookie = null;
		if (cookies != null) {
			for (Cookie cook : cookies) {
				if (COOKIE_NAME.equals(cook.getName())) {
					cookie = cook;
				}
			}
		}
		return ((cookie == null) ? "" : cookie.getValue());
	}

	/**
	 * Save the current player situation and page position.
	 */
	@Override
	public String getBookmarkSaved() {
		JSONArray arrayObj = new JSONArray();
		if (gb.getBookmarks().size() <= 5) {
			gb.saveBookmark();
			getCookieSaved();
			arrayObj.put("OK");
		} else {
			arrayObj.put("KO");
		}
		return arrayObj.toString();
	}

	/**
	 * Save cookie with current game state.
	 */
	private void getCookieSaved() {
		Cookie[] cookies = request.getCookies();
		Cookie cookie = null;
		if (cookies != null) {
			for (Cookie cook : cookies) {
				if (COOKIE_NAME.equals(cook.getName())) {
					System.out.println(cook.getValue());
					cookie = cook;
				}
			}
		}
		// se non ho trovato cookie ne genera uno
		if (cookie == null) {
			cookie = new Cookie(COOKIE_NAME, gb.getBookmark().getString());
			cookie.setMaxAge(100000);// fino a chiusura browser
			response.addCookie(cookie);
		}

	}

	/**
	 * Remove one bookmark from gamebook.
	 */
	@Override
	public String getBookmarkRemoved(String bookmarkId) {
		JSONArray arrayObj = new JSONArray();
		// gb.removeBookmark(new Integer(bookmarkId));
		getCookieRemoved(bookmarkId);
		arrayObj.put("OK");
		return arrayObj.toString();
	}

	/**
	 * Remove named cookie;
	 * 
	 * @param bookmarkId
	 */
	private void getCookieRemoved(String bookmarkId) {
		Cookie[] cookies = request.getCookies();
		Cookie cookie = null;
		if (cookies != null) {
			for (Cookie cook : cookies) {
				if (cook.getName().startsWith(COOKIE_NAME)) {
					cook.setMaxAge(0);
					cookie = cook;
				}
			}
		}
		if (cookie != null) {
			response.addCookie(cookie);
		}
	}

	@Override
	public String getTalks(String pageName) {
		JSONArray arrayObj = new JSONArray();
		Page page = gb.getBook().getPage(pageName);
		if (page == null)
			return arrayObj.toString();
		List<String> talks = page.getTalks();
		if (talks == null)
			return arrayObj.toString();
		for (String talk : talks) {
			arrayObj.put(talk);
		}
		return arrayObj.toString();
	}

	@Override
	public String getSprites(String pageName) {
		JSONArray arrayObj = new JSONArray();
		Page page = gb.getBook().getPage(pageName);
		if (page == null)
			return arrayObj.toString();
		List<String> sprites = page.getSprites();
		if (sprites == null)
			return arrayObj.toString();
		for (String sprite : sprites) {
			arrayObj.put(sprite);
		}
		return arrayObj.toString();
	}

	@Override
	public String getBooks() {
		JSONArray arrayObj = new JSONArray();
		for (GameBookInfo gameBookInfo : gameBookInfos) {
			arrayObj.put(gameBookInfo.getTitle());
		}
		return arrayObj.toString();
	}
}
