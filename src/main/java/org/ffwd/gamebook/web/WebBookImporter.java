package org.ffwd.gamebook.web;

import java.io.InputStream;

import javax.servlet.ServletContext;

import org.ffwd.gamebook.Book;
import org.ffwd.gamebook.BookImporter;

public class WebBookImporter extends BookImporter {

	public Book read(ServletContext context, String bookName) {
		InputStream is =null;
		if (context==null) { //Se stiamo esguento un TEST.
			is = this.getClass().getResourceAsStream("specchio.txt");
		} else {
			is = context
					.getResourceAsStream("/WEB-INF/book/"+bookName);
		}
		return super.read(is);
	}

}
