package org.ffwd.gamebook.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

public class JSONServlet extends HttpServlet {

	private static final long serialVersionUID = -7951904241240413606L;

	int allConnections;
	WebConnection connection;
	Map<String, WebConnection> connections = new HashMap<String, WebConnection>();
	int maxConn = 10;
	int maxMinutesConn = 1;
	Date serverStartDate = new Date();

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		synchronized (this) {

			response.setContentType("application/json");

			String kill = request.getParameter("kill");
			if ("all".equals(kill)) {
				connections.clear();
				PrintWriter out = response.getWriter();
				out.println("connections cleaned.");
				out.flush();
				return;
			}
			if (kill != null) {
				connections.remove(kill);
				PrintWriter out = response.getWriter();
				out.println("connection " + kill + " cleaned.");
				out.flush();
				return;
			}

			String admin = request.getParameter("admin");

			if ("stat".equals(admin)) {
				PrintWriter out = response.getWriter();
				out.println("server started:" + serverStartDate);
				out.println("admin from:" + request.getRemoteAddr());
				out.println("connections since server started:"
						+ allConnections);
				out.println("connections:" + connections.size() + " on max "
						+ maxConn + " for max " + maxMinutesConn + " minutes.");
				Set<String> gameKs = connections.keySet();
				for (String gameK : gameKs) {
					WebConnection con = connections.get(gameK);
					out.println("conn:" + gameK + " started:"
							+ con.getStartDate());
				}
				out.flush();
				return;
			}

			// String gameKey = request.getParameter("game");
			String gameKey = request.getRemoteAddr();

			// gameKey = "2599";
			if (gameKey == null) {
				System.err.println("GAME KEY NULL");
				return;
			}

			synchronized (connections) {
				connection = connections.get(gameKey);
				if (connection == null) {
					cleanConns();
					if (connections.size() >= maxConn) {
						PrintWriter out = response.getWriter();
						out.println("No more connections allowed [" + maxConn
								+ "]! Sorry, try again later.");
						out.flush();
						return;
					}
					allConnections++;
					connection = new WebConnection(getServletContext(),
							request, response);
					connections.put(gameKey, connection);
				}
			}
			connection.setRequest(request);
			connection.setResponse(response);

			String infoPath = request.getPathInfo();
			String[] tokens = null;
			if (infoPath == null) {
				printRequest(request, response);
				return;
			}
			tokens = infoPath.split("/");

			// ricava gli argomenti del metodo.
			@SuppressWarnings("unchecked")
			Map<String, String[]> params = request.getParameterMap();
			int paramsNr = params.size();
			Class<?>[] types = new Class[paramsNr];
			Object[] args = new Object[paramsNr];
			if (paramsNr == 0 || (paramsNr == 1 && params.get("game") != null)) {
				types = null;
				args = null;
			} else {
				for (int i = 0; i < paramsNr; i++) {
					String[] p = params.get("" + i);
					if (p != null && p[0] != null) {
						args[i] = p[0];
						types[i] = String.class;
					}
				}
			}

			// se l'oggetto richiesto è tra quelli conosciuti.
			if (tokens != null && tokens.length > 0 && "game".equals(tokens[1])) {
				// se il metodo richiesto esiste sull'oggetto conosciuto.
				Method method = null;
				try {
					method = connection.getClass().getDeclaredMethod(
							"get" + tokens[2], types);
				} catch (SecurityException e) {
					System.err.println(e.getMessage());
					printRequest(request, response);
					return;
				} catch (NoSuchMethodException e) {
					System.err.println(e.getMessage());
					printRequest(request, response);
					return;
				}
				if (method != null) {
					// invoca e ritorna in JSON la risposta del metodo.
					try {
						Object obj = method.invoke(connection, args);
						boolean isJsonResponse = false;
						if (obj instanceof String) {
							String str = (String) obj;
							if (str.startsWith("[")) {
								isJsonResponse = true;
								System.err.println(str);
								PrintWriter out = response.getWriter();
								out.println(str);
							}
						}
						if (!isJsonResponse) {
							JSONArray arrayObj = new JSONArray();
							arrayObj.put(obj);
							if ("Start".equals(tokens[2])) {
								response.sendRedirect("../../play-" + args[0]
										+ ".html");
								return;
							}
							if ("Restart".equals(tokens[2])) {
								response.sendRedirect("../../books/index.html");
								return;
							}
							PrintWriter out = response.getWriter();
							out.println(arrayObj);
						}
					} catch (IllegalArgumentException e) {
						System.err.println("JDONServlet>" + e.getMessage());
						printRequest(request, response);
						return;
					} catch (IllegalAccessException e) {
						System.err.println("JDONServlet>" + e.getMessage());
						printRequest(request, response);
						return;
					} catch (InvocationTargetException e) {
						e.printStackTrace();
						printRequest(request, response);
						return;
					}
				}
			} else {
				printRequest(request, response);
			}
		}// synchronized

	}

	private void printRequest(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String contextPath = request.getContextPath();
		String infoPath = request.getPathInfo();
		String transPath = request.getPathTranslated();

		JSONArray arrayObj = new JSONArray();
		arrayObj.put("ContextPath:[" + contextPath + "]");
		arrayObj.put("InfoPath:[" + infoPath + "]");
		arrayObj.put("TransPath:[" + transPath + "]");

		PrintWriter out = response.getWriter();
		out.println(arrayObj);
	}

	// private void printRequestException(Exception e) {
	// PrintWriter s = new PrintWriter(new StringWriter());
	// e.printStackTrace(s);
	//
	// System.out.println(">>>" + s.toString() + "<<<");
	//
	// }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		synchronized (this) {

			response.setContentType("application/json");

			String infoPath = request.getPathInfo();

			String[] tokens = null;
			if (infoPath == null) {
				printRequest(request, response);
				return;
			}
			tokens = infoPath.split("/");

			// se l'oggetto richiesto è tra quelli conosciuti.
			if (tokens != null && "game".equals(tokens[1])) {

				// ricava gli argomenti del metodo.
				@SuppressWarnings("unchecked")
				Map<String, String[]> params = request.getParameterMap();
				Class<?>[] types = new Class[params.size()];
				Object[] args = new Object[params.size()];
				for (int i = 0; i < params.size(); i++) {
					String[] p = params.get("" + i);
					if (p != null && p[0] != null) {
						args[i] = p[0];
						types[i] = String.class;
					}
				}

				// ricava il gameseed
				// String gameKey = request.getParameter("game");
				String gameKey = request.getRemoteAddr();

				synchronized (connections) {
					connection = connections.get(gameKey);
					if (connection == null) {
						cleanConns();
						if (connections.size() >= maxConn) {
							PrintWriter out = response.getWriter();
							out.println("No more connections allowed ["
									+ maxConn + "]! Sorry, try again later.");
							out.flush();
							return;
						}
						allConnections++;
						connection = new WebConnection(getServletContext(),
								request, response);
						connections.put(gameKey, connection);
					}
				}
				connection.setRequest(request);
				connection.setResponse(response);

				// se il metodo richiesto esiste sull'oggetto conosciuto.
				Method method = null;
				try {
					method = connection.getClass().getDeclaredMethod(
							"set" + tokens[2], types);
				} catch (SecurityException e) {
					System.err.println(e.getMessage());
					printRequest(request, response);
					return;
				} catch (NoSuchMethodException e) {
					System.err.println(e.getMessage());
					printRequest(request, response);
					return;
				}
				if (method != null) {
					// invoca e ritorna in JSON la risposta del metodo.
					try {
						Object obj = method.invoke(connection, args);
						JSONArray arrayObj = new JSONArray();
						arrayObj.put(obj);
						PrintWriter out = response.getWriter();
						out.println(arrayObj);
					} catch (IllegalArgumentException e) {
						System.err.println(e.getMessage());
						printRequest(request, response);
						return;
					} catch (IllegalAccessException e) {
						System.err.println(e.getMessage());
						printRequest(request, response);
						return;
					} catch (InvocationTargetException e) {
						System.err.println(e.getMessage());
						printRequest(request, response);
						return;
					}
				}
			} else {
				printRequest(request, response);
			}
		}// synchronized

	}

	@Override
	protected void doDelete(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		printRequest(request, response);
	}

	@Override
	protected void doPut(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		printRequest(request, response);
	}

	private void cleanConns() {
		List<String> toRemove = null;
		for (String conKey : connections.keySet()) {
			if (conKey != null) {
				WebConnection con = connections.get(conKey);
				Date now = new Date();
				if (now.getTime() - con.getStartDate().getTime() > 1000 * 60 * maxMinutesConn) {
					if (toRemove == null)
						toRemove = new ArrayList<String>();
					toRemove.add(conKey);
				}
			}
		}
		if (toRemove != null) {
			for (String conKey : toRemove) {
				if (conKey != null)
					connections.remove(conKey);
			}
		}
	}
}