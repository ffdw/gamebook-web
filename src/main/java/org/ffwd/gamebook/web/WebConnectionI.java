package org.ffwd.gamebook.web;

public interface WebConnectionI {

	public String getRestart(String gameName);

	public String getIntroPage();

	public String getPage(String pageName);

	public String getStartedOn();

	public String getPlayer();

	public String getFight(String pageName);

	public String getInventary();

	public String getUse(String oggetto);

	public String getInUse();

	public String getGive(String oggetto);

	public String getTake(String oggetto);

	public String getLearn(String info);

	public String getTell(String info);

	public String getBookmarks();

	public String getBookmark(String bookmardId);

	public String getBookmarkSaved();

	public String getBookmarkRemoved(String bookmardId);

	public String getTalks(String pageName);

	public String getSprites(String pageName);

	public String getBooks();
}