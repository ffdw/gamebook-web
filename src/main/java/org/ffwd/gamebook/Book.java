package org.ffwd.gamebook;

import java.util.HashMap;
import java.util.Map;

public class Book {

	private String title;
	private Map<String, Page> pages = new HashMap<String, Page>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void addPage(String key, Page page) {
		pages.put(key, page);
	}

	public Page getPage(String key) {
		return pages.get(key);
	}

	public int getNrPages() {
		return pages.size();
	}
}
