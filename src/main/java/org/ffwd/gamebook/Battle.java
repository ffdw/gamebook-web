package org.ffwd.gamebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Battle {

	private static final String FIGHT_PREFIX_FORMAT = "%03d";
	public static final String FIGHT_PREFIX_WIN = ">>>";
	public static final String FIGHT_PREFIX_LOOSE = "<<<";
	NotPlayer np;
	Player p;

	public void fight() {
		np = new NotPlayer();
		p = new Player().randomize();
		fight(p, np);
	}

	public void fight(Player p, NotPlayer np) {
		InputStreamReader converter = new InputStreamReader(System.in);
		BufferedReader in = new BufferedReader(converter);
		fight(p, np, in);
	}

	public boolean fight(Player p, NotPlayer np, BufferedReader in) {
		this.p = p;
		this.np = np;
		String CurLine = ""; // Line read from standard in

		System.out.println("Fight p.level:" + p.getLevel() + " np.level:"
				+ np.getLevel());
		System.out.println("Enter a line of text (type 'quit' to exit): ");

		boolean exit = false;
		boolean win = false;
		while (!exit) {
			try {
				round();
				CurLine = in.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// TODO add USE <OGGETTO>
			if (CurLine.equals("quit")) {
				System.out.println("You quit! ");
				exit = true;
			} else if (CurLine.equals("fuga")) {
				System.out.println("You try to escape...");
				if (fuga())
					exit = true;
			} else if (CurLine.equals("mana")) {
				System.out.println("You use mana...");
				mana(1);
			} else if (CurLine.equals("mana2")) {
				System.out.println("You use mana...");
				mana(2);
			} else if (CurLine.equals("mana3")) {
				System.out.println("You use mana...");
				mana(3);
			} else if (CurLine.equals("mani")) {
				System.out.println("...ok now you use only your hands.");
				p.inUso = null;
			} else {
				for (Oggetto o : p.inventario) {
					if (o.name.equals(CurLine)) {
						System.out.println("..ok, you now use " + o.name);
						p.inUso = o;
					}
				}
			}

			if (p.isDead()) {
				System.out.println("You have loose.");
				exit = true;
				win = false;
			}
			if (np.isDead()) {
				p.mana++;
				p.experience += 5;
				System.out.println("You have win!");
				exit = true;
				win = true;
			}

		}
		return win;
	}

	public void setNotPlayer(NotPlayer np) {
		this.np = np;
	}

	public void setPlayer(Player p) {
		this.p = p;
	}

	public String round() {
		String esito = "";
		String text = "";// String.format("PLAYER: %s %s:%d", p.getStamina(),
							// np.getNome(), np.getStamina());
		System.out.println(text);
		esito += text;
		if (p.getStamina() <= 0) {
			esito += "hai perso";
			esito = FIGHT_PREFIX_LOOSE + esito;
			return esito;
		}
		if (np.getStamina() <= np.getResistenza()) {
			esito += "hai vinto";
			esito = FIGHT_PREFIX_WIN + esito;
			return esito;
		}
		int d3 = Dice.roll.get6();
		int d4 = Dice.roll.get6();
		int attackStrength = d3 + d4 + np.getAbilita();
		int d1 = Dice.roll.get6();
		int d2 = Dice.roll.get6();
		String diceresult = String.format(FIGHT_PREFIX_FORMAT, d1 + d2);
		esito = diceresult + esito;
		int defenseStrength = d1 + d2 + p.getAbilita();
		// eventuale bonus abilita d'oggetto in uso.
		if (p.inUso != null) {
			if (p.inUso.disponibilita == 0) {
				String x = "Oggetto " + p.inUso.name
						+ " non più usabile. Lo butti via.";
				System.out.println(x);
				esito += x;
				p.remOggetto(p.inUso);
				p.inUso = null;
			} else {
				p.inUso.consume();
				defenseStrength += p.inUso.getAbilita();
				p.mana += p.inUso.getManaUso();
				p.stamina += p.inUso.getStamina();
			}
		}
		int luckyStar = Dice.roll.get6() + Dice.roll.get6();
		String x = String
				.format("<br>PLAYER dice=%d,stamina=%d CREATURA dado=%d,stamina=%d <br>",
						(d1 + d2), p.getStamina(), (d3 + d4), np.getStamina());
		System.out.format(x);
		esito += x;
		if (defenseStrength > attackStrength) {
			// int danno = (4 + Dice.roll.get(p.getLevel()));
			int danno = defenseStrength - attackStrength;
			x = " you have wounded it -" + danno;
			System.out.println(x);
			esito += x;
			// attacco normale
			np.stamina -= danno;
			// evetuale arma;
			if (p.inUso != null) {
				if (p.inUso.getAttacco() > 0) {
					x = " You used the " + p.inUso.name + " -"
							+ p.inUso.getAttacco();
					System.out.println(x);
					esito += x;
					np.stamina -= p.inUso.consumeAttacco();
				}
			}
			// eventuale colpo di fortuna
			if (p.luck < luckyStar) {
				System.out.println(" you are lucky.");
				np.stamina -= 4;
			}
		} else if (attackStrength > defenseStrength + 1) {
			// int danno = (2 + Dice.roll.get(np.getLevel()));
			int danno = attackStrength - defenseStrength;
			x = " it has wonded you -" + danno;
			System.out.println(x);
			esito += x;
			// normale danno
			p.stamina -= danno;
			// eventuale scudo;
			if (p.inUso != null) {
				if (p.inUso.getDifesa() > 0) {
					x = " You used the " + p.inUso.name + " -"
							+ p.inUso.getDifesa();
					System.out.println(x);
					esito += x;
					p.stamina += p.inUso.consumeDifesa();
				}
			}
			// eventuale colpo di sfortuna
			if (p.luck < luckyStar) {
				x = " you are not lucky, deep wound -2.";
				System.out.println(x);
				esito += x;
				p.stamina -= 2;
			}
		} else {
			// pari e patta
			x = " second chance.";
			System.out.println(x);
			esito += x;
		}
		return esito;
	}

	public void cattivaSorpresa() {

	}

	public boolean fuga() {
		return true;

	}

	public void mana(int points) {
		p.mana -= points;
		System.out.println("your mana level now is:" + p.mana);
		if (p.mana < 0) {
			System.out.println(" sorry, you don't have enough mana!");
			return;
		}
		int luckystar = Dice.roll.get6() + Dice.roll.get6();
		if (p.luck >= luckystar) {
			p.addStamina(3 * points);
			System.out.println("you energize +" + (3 * points) + "!");
		} else {
			System.out.println(" sorry, you can't concentrate!");
		}
	}

	public static void main(String[] args) {
		new Battle().fight();
	}
}
