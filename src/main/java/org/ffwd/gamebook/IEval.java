package org.ffwd.gamebook;

import java.util.Map;

public interface IEval {

	public Object eval(String Script) throws Exception;

	public Object eval(String Script, Map<String, Object> arguments)
			throws Exception;
}
