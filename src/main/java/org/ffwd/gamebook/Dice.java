package org.ffwd.gamebook;

import java.util.Random;

public enum Dice {
	roll;

	public Random r = new Random();

	public int get6() {
		return r.nextInt(5) + 1;
	}

	public int get(int max) {
		return r.nextInt(max) + 1;
	}
}
