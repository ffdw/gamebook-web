package org.ffwd.gamebook;

import java.io.BufferedReader;
import java.io.IOException;

public class CreazionePlayer {

	static public Player build(BufferedReader in, Book book) {
		Player p = new Player().randomize();

		// nome
		System.out.println("Quale nome scegli per il tuo personaggio?");
		String name = "no-name";
		name = readtext(in);
		p.setNome(name);

		// base
		System.out.println(book.getPage("BASE"));
		System.out.println("Caratteristiche base:");
		System.out.println("Stamina: " + p.stamina);
		System.out.println("Mana: " + p.mana);
		System.out.println("Abilita: " + p.abilita);
		readline(in);

		// gilde
		System.out.println(book.getPage("GILDE"));
		System.out.println("Quale gilda scegli?");
		String gilda = readtext(in).toUpperCase();
		p.setGilda(gilda);

		// speciali
		System.out.println(book.getPage("SPECIALI " + gilda));
		System.out.println("Quale caratteristica speciale scegli?");
		readline(in);

		// oggetti
		Oggetto o = new Oggetto("spada", 1000, 5, 5, 0);
		o.setAttacco(5);
		o.setDifesa(2);
		p.addOggetto(o);
		System.out.println("INVENTARIO");
		for (Oggetto oi : p.getInventario()) {
			System.out.println(oi);
		}
		System.out.println("Premi INVIO per proseguire.");
		readline(in);

		// magie
		// TODO
		return p;
	}

	private static String readtext(BufferedReader in) {
		String text = null;
		try {
			do {
				text = in.readLine();
			} while ("".equals(text));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}

	private static String readline(BufferedReader in) {
		String text = null;
		try {
			text = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}

}
