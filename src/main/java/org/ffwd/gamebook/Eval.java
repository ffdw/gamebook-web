package org.ffwd.gamebook;

import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Eval implements IEval {

	protected ScriptEngineManager factory = new ScriptEngineManager();
	protected ScriptEngine engine = factory.getEngineByName("JavaScript");

	@Override
	public Object eval(String script) throws Exception {
		Object result = null;
		try {
			result = engine.eval(script);
		} catch (ScriptException e) {
			throw new Exception(e.getMessage());
		}
		return result;
	}

	@Override
	public Object eval(String script, Map<String, Object> arguments)
			throws Exception {
		for (Map.Entry<String, Object> argument : arguments.entrySet()) {
			engine.put(argument.getKey(), argument.getValue());
		}
		return eval(script);
	}

}
