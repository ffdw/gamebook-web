// *******************  PROTOTYPE

if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

//*******************  WHEEL

var hide = function hideWheel(esito,directionlink,directiondesc) {
	setTimeout(function(){		
			console.log('hideWheel');
			$( "#wheel" ).fadeOut(); 
			$("#texto").append("<hr>"+esito);
			$("#texto").append("<br><a onclick='updateText(\""+directionlink+"\");return false;' href='#'><img style='vertical-align:middle' src='img/ButtonG.jpg'/> "+directiondesc+"<a/>"); 
		},7000);
	};

function spinWheel(dicevalue,esito,dirlink,dirdesc) {
	$("#wheel").rotate({ 
		bind: 
		{ 
		click : function() { 
		 	//var randomangle = Math.floor(Math.random()*13)*30;
		 	var randomangle = ((dicevalue*-1)+1)*30;
			$(this).rotate({
				        duration:6000,
			        	angle: 0,
			        	callback: hide(esito,dirlink,dirdesc), 
			            animateTo:7200+randomangle});
			},
		} 
		
		});
	
}

//*******************  GENERAL
var dirs=[];

function updateText(pagetitle) {
	dirs=[];
    $.getJSON('/gamebook-web/do/game/Page?0='+pagetitle, function(str) {
    	var texto = str[0];
    	var topimage = str[1];
    	var pagetype = str[2]; 
    	$("#pagetitle").html("<h2> &nbsp;&nbsp;&nbsp;"+pagetitle+" </h2>");
    	//update bg-img div content
    	if (topimage.endsWith('.mp4')) {    		
    		$("#bg-img-id").html("<video id='sampleMovie' src='video/"+topimage+"' poster='img/sintel.png' width='100%' controls></video>");
    	} else {
    		$("#bg-img-id").html("<img id='topimage' src='img/"+topimage+"' alt='Background Image' />");
    	}
    	
    	// wheel page
    	if (pagetype==="wheel") {
    		$( "#wheel" ).fadeIn();
        	var dicevalue = str[3];
        	var esito = str[4]; 
        	var dirlink = str[5];
        	var dirdesc = str[6];
        	spinWheel(dicevalue,esito,dirlink,dirdesc);
    	}
    	// form page
    	else if (pagetype==="form") {
    		
    	}
    	else {
    		var nrDirections = (str.length-2)/2; 
    		for (var i=1;i<=nrDirections;i++) {
    			var j= 2*i;
    			dirs[i]= "<br><a onclick='updateText(\""+str[j]+"\")' href='#'><img style='vertical-align:middle' src='img/ButtonG.jpg'/> "+str[j+1]+"<a/>"; 
    		}    		
    	}
    	$("#texto").html(texto);
    	updateTalks(pagetitle);
    });
}

// *******************  TALKS

var talks;

function updateTalks(pagetitle) {
    $.getJSON('/gamebook-web/do/game/Talks?0='+pagetitle, function(str) {
    	talks = str;
    	if (talks.length>0) {    		
    		updateTalk(0);
    	} else {
    		appendDirs();
    	}
    });
	
}

function updateTalk(pos) {
	talk  = talks[pos];
	if (pos>0) {		
		$("#talk"+pos).attr("onclick","return false;");
		$("#talkdiv"+pos).attr("class","talked");
	}
	pos = pos+1;
	if(pos!=talks.length) {		
		$("#texto").append("<div class='talk' id='talkdiv"+pos+"' ><a id='talk"+pos+"' href='' onclick='updateTalk("+pos+");return false;'>"+talk+"</a></div>");
	} else {
		$("#texto").append("<div class='talk' >"+talk+"</div>");
		appendDirs();
	}
}

function appendDirs() {
	for(i=0;i<dirs.length;i++) {
		$("#texto").append(dirs[i]);
	}
}
//*******************  BOOKMARKS

function bookmark(pos) {
	 $.getJSON('/gamebook-web/do/game/Bookmark','0='+pos, function(str) {		 
		 console.log('bookmark'+str[0]);
		 updateText(str[0]);
		 $( "#bookmarksbg" ).slideToggle();
		 $( "#page" ).slideToggle();
	 });
	
}

function addBookmark() {
	 $.getJSON('/gamebook-web/do/game/BookmarkSaved', function(str) {
	   $.getJSON('/gamebook-web/do/game/Bookmarks', function(str) {
		   if (str.length<5) {				   
			   $("#bookmarklist").html('<li>current page <a href="#" onClick="addBookmark();return false;">+</a> </li>');
		   } else {
			   $("#bookmarklist").html('');
		   }
		   for (var i = 0; i < str.length; ++i) {
			   $("#bookmarklist").append('<li><a href="#" onClick="bookmark('+i+');return false;">'+str[i]+'</a> <a href="#" onClick="removeBookmark('+i+');return false;">-</a> </li>');
		   }
	   });
	 });
	
}

function removeBookmark(pos) {
	 $.getJSON('/gamebook-web/do/game/BookmarkRemoved','0='+pos, function(str) {
		   $.getJSON('/gamebook-web/do/game/Bookmarks', function(str) {
			   if (str.length<5) {				   
				   $("#bookmarklist").html('<li>current page <a href="#" onClick="addBookmark();return false;">+</a> </li>');
			   } else {
				   $("#bookmarklist").html('');
			   }
			   for (var i = 0; i < str.length; ++i) {
				   $("#bookmarklist").append('<li><a href="#" onClick="bookmark('+i+');return false;">'+str[i]+'</a> <a href="#" onClick="removeBookmark('+i+');return false;">-</a> </li>');
			   }
		   });
		 });	
}

//*******************  STARTUP

$(document).ready(function() {
	$("#wheel").fadeOut();
	$(".addbookmark").click(function() {
		addBookmark();
		return false;
	});
	$("#restart").click(function() {
		   $.getJSON('/gamebook-web/do/game/Restart','0=specchio.txt', function(str) {
			   //start page;
			   updateText('INTRO');
		   });	
	});
	$( "#bookmarksbg" ).slideToggle();
	$( "#statusbg" ).slideToggle();
	$( "#eyetrigger" ).click(function() {
		$('.header').slideToggle();
	});
	$( "#statustrigger" ).click(function() {
		   $.getJSON('/gamebook-web/do/game/Player', function(str) {
			    var abilita = str[1];
			    $("#abilita").html("Abilita: "+abilita);
			    var stamina = str[2];
			    $("#stamina").html("Stamina: "+stamina);
			    var mana = str[3];
			    $("#mana").html("Mana: "+mana);
			    var expe = str[4];
			    $("#expe").html("Experience: "+expe);
			    var gilda = str[5];
			    $("#gilda").html("Gilda: "+gilda);
				$( "#statusbg" ).slideToggle( "slow", function() {
					// Animation complete.
					});
				$( "#page" ).slideToggle( "slow", function() {
						// Animation complete.
					});
		   });	
		
		
		});
	$( "#bookmarkstrigger" ).click(function() {
		   $.getJSON('/gamebook-web/do/game/Bookmarks', function(str) {
			   if (str.length<5) {
				   $("#bookmarklist").html('<li>current page <a href="#" onClick="addBookmark();return false;">+</a> </li>');
			   } else {
				   $("#bookmarklist").html('');
			   }
			   for (var i = 0; i < str.length; ++i) {
				   $("#bookmarklist").append('<li><a href="#" onClick="bookmark('+i+');return false;">'+str[i]+'</a> <a href="#" onClick="removeBookmark('+i+');return false;">-</a> </li>');
			   }
			   $( "#bookmarksbg" ).slideToggle( "slow", function() {
				   // Animation complete.
			   });
			   $( "#page" ).slideToggle( "slow", function() {
				   // Animation complete.
			   });
		   });
		});
	//Start page INTRO;
	updateText('INTRO');
	            
});

//*******************  EFFETCS

	(function() {

		// detect if IE : from http://stackoverflow.com/a/16657946		
		var ie = (function(){
			var undef,rv = -1; // Return value assumes failure.
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf('MSIE ');
			var trident = ua.indexOf('Trident/');

			if (msie > 0) {
				// IE 10 or older => return version number
				rv = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
			} else if (trident > 0) {
				// IE 11 (or newer) => return version number
				var rvNum = ua.indexOf('rv:');
				rv = parseInt(ua.substring(rvNum + 3, ua.indexOf('.', rvNum)), 10);
			}

			return ((rv > -1) ? rv : undef);
		}());


		// disable/enable scroll (mousewheel and keys) from http://stackoverflow.com/a/4770179					
		// left: 37, up: 38, right: 39, down: 40,
		// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
		var keys = [32, 37, 38, 39, 40], wheelIter = 0;

		function preventDefault(e) {
			e = e || window.event;
			if (e.preventDefault)
			e.preventDefault();
			e.returnValue = false;  
		}

		function keydown(e) {
			for (var i = keys.length; i--;) {
				if (e.keyCode === keys[i]) {
					preventDefault(e);
					return;
				}
			}
		}

		function touchmove(e) {
			preventDefault(e);
		}

		function wheel(e) {
			// for IE 
			//if( ie ) {
				//preventDefault(e);
			//}
		}

		function disable_scroll() {
			window.onmousewheel = document.onmousewheel = wheel;
			document.onkeydown = keydown;
			document.body.ontouchmove = touchmove;
		}

		function enable_scroll() {
			window.onmousewheel = document.onmousewheel = document.onkeydown = document.body.ontouchmove = null;  
		}

		var docElem = window.document.documentElement,
			scrollVal,
			isRevealed, 
			noscroll, 
			isAnimating,
			container = document.getElementById( 'container' ),
			//header = document.getElementById( 'header' ),
			trigger = container.querySelector( 'button.trigger' );

		function scrollY() {
			return window.pageYOffset || docElem.scrollTop;
		}
		
		function scrollPage() {
			scrollVal = scrollY();
			
			if( noscroll && !ie ) {
				if( scrollVal < 0 ) return false;
				
				// keep it that way
				window.scrollTo( 0, 0 );
			}

			//console.log(scrollVal);
			//$('.header').fadeOut();
			
			if( classie.has( container, 'notrans' ) ) {
				classie.remove( container, 'notrans' );
				return false;
			}

			if( isAnimating ) {
				return false;
			}
			
			if( scrollVal <= 0 && isRevealed ) {
				$('.header').fadeIn();
				toggle(0);
			}
			else if( scrollVal > 0 && !isRevealed ){
				$('.header').fadeOut();
				toggle(1);
			}
		}

		function toggle( reveal ) {
			isAnimating = true;
			
			if( reveal ) {
				classie.add( container, 'modify' );
			}
			else {
				noscroll = true;
				disable_scroll();
				classie.remove( container, 'modify' );
			}

			// simulating the end of the transition:
			setTimeout( function() {
				isRevealed = !isRevealed;
				isAnimating = false;
				if( reveal ) {
					noscroll = false;
					enable_scroll();
				}
			}, 600 );
		}

		// refreshing the page...
		var pageScroll = scrollY();
		noscroll = pageScroll === 0;
		
		disable_scroll();
		
		if( pageScroll ) {
			isRevealed = true;
			classie.add( container, 'notrans' );
			classie.add( container, 'modify' );
		}
		
		window.addEventListener( 'scroll', scrollPage );
		trigger.addEventListener( 'click', function() { toggle( 'reveal' ); } );
	})();
