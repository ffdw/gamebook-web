// *******************  PROTOTYPE

if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

//*******************  WHEEL

var hide = function hideWheel(esito,directionlink,directiondesc) {
	setTimeout(function(){		
			console.log('hideWheel');
			$( "#wheel" ).fadeOut(); 
			$("#texto").append("<hr>"+esito);
			$("#texto").append("<br><a onclick='updateText(\""+directionlink+"\");return false;' href='#'><img style='vertical-align:middle' src='img/ButtonG.jpg'/> "+directiondesc+"<a/>"); 
		},7000);
	};

function spinWheel(dicevalue,esito,dirlink,dirdesc) {
	$("#wheel").rotate({ 
		bind: 
		{ 
		click : function() { 
		 	//var randomangle = Math.floor(Math.random()*13)*30;
		 	var randomangle = ((dicevalue*-1)+1)*30;
			$(this).rotate({
				        duration:6000,
			        	angle: 0,
			        	callback: hide(esito,dirlink,dirdesc), 
			            animateTo:7200+randomangle});
			},
		} 
		
		});
	
}

//*******************  GENERAL
var dirs=[];

function updateText(pagetitle) {
	dirs=[];
    $.getJSON('/gamebook-web/do/game/Page?0='+pagetitle, function(str) {
    	var texto = str[0];
    	var topimage = str[1];
    	var pagetype = str[2]; 
    	$("#pagetitle").html("<h2> &nbsp;&nbsp;&nbsp;"+pagetitle+" </h2>");
    	//update bg-img div content
    	if (topimage.endsWith('.mp4')) {    		
    		//$("#bg-img-id").html("<video id='sampleMovie' src='video/"+topimage+"' poster='img/sintel.png' width='100%' controls></video>");
    	} else {
    		//$("#bg-img-id").html("<img id='topimage' src='img/"+topimage+"' alt='Background Image' />");
    		$("#container").css("background","url(img/"+topimage+") no-repeat center center fixed");
    		$("#container").css("height","100%");
    		$("#container").css("-moz-background-size","cover");
    		$("#container").css("background-size","cover");
    	}
    	
    	// wheel page
    	if (pagetype==="wheel") {
    		$( "#wheel" ).fadeIn();
        	var dicevalue = str[3];
        	var esito = str[4]; 
        	var dirlink = str[5];
        	var dirdesc = str[6];
        	spinWheel(dicevalue,esito,dirlink,dirdesc);
    	}
    	// form page
    	else if (pagetype==="form") {
    		
    	}
    	else {
    		var nrDirections = (str.length-2)/2; 
    		for (var i=1;i<=nrDirections;i++) {
    			var j= 2*i;
    			dirs[i]= "<br><a onclick='updateText(\""+str[j]+"\");return false;' href=''><img style='vertical-align:middle' src='img/button-small.png'/> "+str[j+1]+"<a/>"; 
    		}    		
    	}
    	$("#texto").html(texto);
    	updateTalks(pagetitle);
    });
}

// *******************  TALKS

var talks;

function updateTalks(pagetitle) {
    $.getJSON('/gamebook-web/do/game/Talks?0='+pagetitle, function(str) {
    	talks = str;
    	if (talks.length>0) {    		
    		updateTalk(0);
    	} else {
    		appendDirs();
    	}
    });
	
}

function updateTalk(pos) {
	talk  = talks[pos];
	if (pos>0) {		
		$("#talk"+pos).attr("onclick","return false;");
		$("#talkdiv"+pos).attr("class","talked");
	}
	pos = pos+1;
	if(pos!=talks.length) {		
		$("#texto").append("<div class='talk' id='talkdiv"+pos+"' ><a id='talk"+pos+"' href='' onclick='updateTalk("+pos+");return false;'>"+talk+"</a></div>");
	} else {
		$("#texto").append("<div class='talk' >"+talk+"</div>");
		appendDirs();
	}
}

function appendDirs() {
	for(i=0;i<dirs.length;i++) {
		$("#texto").append(dirs[i]);
	}
}
//*******************  BOOKMARKS

function bookmark(pos) {
	 $.getJSON('/gamebook-web/do/game/Bookmark','0='+pos, function(str) {		 
		 console.log('bookmark'+str[0]);
		 updateText(str[0]);
		 $( "#bookmarksbg" ).slideToggle();
		 $( "#page" ).slideToggle();
	 });
	
}

function addBookmark() {
	 $.getJSON('/gamebook-web/do/game/BookmarkSaved', function(str) {
	   $.getJSON('/gamebook-web/do/game/Bookmarks', function(str) {
		   if (str.length<5) {				   
			   $("#bookmarklist").html('<li>current page <a href="#" onClick="addBookmark();return false;">+</a> </li>');
		   } else {
			   $("#bookmarklist").html('');
		   }
		   for (var i = 0; i < str.length; ++i) {
			   $("#bookmarklist").append('<li><a href="#" onClick="bookmark('+i+');return false;">'+str[i]+'</a> <a href="#" onClick="removeBookmark('+i+');return false;">-</a> </li>');
		   }
	   });
	 });
	
}

function removeBookmark(pos) {
	 $.getJSON('/gamebook-web/do/game/BookmarkRemoved','0='+pos, function(str) {
		   $.getJSON('/gamebook-web/do/game/Bookmarks', function(str) {
			   if (str.length<5) {				   
				   $("#bookmarklist").html('<li>current page <a href="#" onClick="addBookmark();return false;">+</a> </li>');
			   } else {
				   $("#bookmarklist").html('');
			   }
			   for (var i = 0; i < str.length; ++i) {
				   $("#bookmarklist").append('<li><a href="#" onClick="bookmark('+i+');return false;">'+str[i]+'</a> <a href="#" onClick="removeBookmark('+i+');return false;">-</a> </li>');
			   }
		   });
		 });	
}

//*******************  STARTUP

$(document).ready(function() {
	$("#wheel").fadeOut();
	$(".addbookmark").click(function() {
		addBookmark();
		return false;
	});
	$("#restart").click(function() {
		   $.getJSON('/gamebook-web/do/game/Restart','0=specchio.txt', function(str) {
			   //start page;
			   updateText('INTRO');
		   });	
	});
	$( "#bookmarksbg" ).slideToggle();
	$( "#statusbg" ).slideToggle();
	$( "#eyetrigger" ).click(function() {
		$('.header').slideToggle();
	});
	$( "#statustrigger" ).click(function() {
		   $.getJSON('/gamebook-web/do/game/Player', function(str) {
			    var abilita = str[1];
			    $("#abilita").html("Abilita: "+abilita);
			    var stamina = str[2];
			    $("#stamina").html("Stamina: "+stamina);
			    var mana = str[3];
			    $("#mana").html("Mana: "+mana);
			    var expe = str[4];
			    $("#expe").html("Experience: "+expe);
			    var gilda = str[5];
			    $("#gilda").html("Gilda: "+gilda);
				$( "#statusbg" ).slideToggle( "slow", function() {
					// Animation complete.
					});
				$( "#page" ).slideToggle( "slow", function() {
						// Animation complete.
					});
		   });	
		
		
		});
	$( "#bookmarkstrigger" ).click(function() {
		   $.getJSON('/gamebook-web/do/game/Bookmarks', function(str) {
			   if (str.length<5) {
				   $("#bookmarklist").html('<li>current page <a href="#" onClick="addBookmark();return false;">+</a> </li>');
			   } else {
				   $("#bookmarklist").html('');
			   }
			   for (var i = 0; i < str.length; ++i) {
				   $("#bookmarklist").append('<li><a href="#" onClick="bookmark('+i+');return false;">'+str[i]+'</a> <a href="#" onClick="removeBookmark('+i+');return false;">-</a> </li>');
			   }
			   $( "#bookmarksbg" ).slideToggle( "slow", function() {
				   // Animation complete.
			   });
			   $( "#page" ).slideToggle( "slow", function() {
				   // Animation complete.
			   });
		   });
		});
	//Start page INTRO;
	updateText('INTRO');
	            
});
