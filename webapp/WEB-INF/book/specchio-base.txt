LO SPECCHIO DI EBLIS

INTRO
Quello che stai per leggere non è un normale libro. 
Scoprirai nelle pagine che seguiranno come dare forma al tuo
personaggio e come agire nel corso dell’avventura. 
Perché il racconto abbia un inizio, uno svolgimento e 
una fine dovrai seguire le indicazioni che ti verranno 
date alla fine di ogni paragrafo.
Sarai tu a decidere le sorti del personaggio che andrai a
incarnare.
--->PROLOGO



PROLOGO
“Da tempo, come sai, io e gli altri Maestri Anziani stiamo
cercando disperatamente di porre rimedio alla situazione
orribile in cui versa il regno. Ma non è facile... per un
brigante che eliminiamo ne spuntano altri dieci, per ogni
villaggio che salviamo ne vengono distrutti tanti altri. Siamo
troppo pochi per difendere Sirdania. Per questo abbiamo
deciso di porre tutte le nostre energie nella soluzione del
problema... alla radice.”
“Alla radice?”
“Mano Nera. Dobbiamo eliminare Mano Nera in persona.”
--->1



1
Il Maestro Anziano ti accompagna in una delle celle dei
monaci di Wigga, per darti le ultime istruzioni prima della
partenza.
"L'entrata al covo di Mano Nera si trova sulle Colline di
Whyred, a circa due giorni di cammino a nord del villaggio
di Festerheim. Il sindaco, Pirian, è un mio caro amico, ed è
stato informato della tua missione; sia lui che i suoi
concittadini ti offriranno tutto il loro appoggio e ti
indicheranno la strada per le colline."
"Dove si trova esattamente l'entrata al covo?"
"Chiudi gli occhi e lo vedrai."
Obbedisci all'ordine del Maestro. Dopo qualche istante,
un'immagine balena nella tua mente.
Un sentiero sulle colline, attorniato da rovi e erbacce. Curva
bruscamente a sinistra seguendo il bordo di un ampio
crepaccio, e procede in lontananza, fra mille svolte, salendo
verso le pendici delle Colline Nebbiose... d'un tratto appare
un'enorme roccia rossastra, dalla forma stranamente simile
a un'aquila con le ali spiegate. Un simbolo nero è dipinto
alla sua base, ma non riesci a distinguerlo...
Improvvisamente, un bagliore illumina il fondo del
crepaccio. Il sentiero crolla, sprofondi verso il basso, giù,
nelle profondità della terra... la luce è abbagliante...
"Basta così!"
Ti scuoti all'improvviso. Sei di nuovo nella cella del
monastero, madido di sudore.
"Il... il crepaccio..."
"È solo un'illusione. Se lo oltrepasserai nel punto esatto che
ti ho mostrato, ti condurrà al covo di Mano Nera.""Come farò... a passare?"
"Il medaglione – afferma il Maestro Anziano, indicando
l’artefatto che pende dal tuo collo. - Quando sarai sul luogo,
saprai come usarlo."
Annuisci, ansimando. Mentre stai ancora recuperando le
energie, il tuo tutore ti indica il tavolino di fronte a te. Noti
che ci sono alcune razioni di cibo, un'arma e una boccetta di
vetro; si volta, afferra la boccetta e te la porge. Contiene un
liquido incolore che emette degli strani bagliori nel buio
della cella.
"È la pozione della Vita, acqua benedetta proveniente dalla
fonte sacra di Wigga. Custodiscila con cura: se la berrai,
recupererai istantaneamente tutte le forze e verrai curato da
tutte le malattie. Oltre a questa, hai tutto ciò che abbiamo
potuto raccogliere per il viaggio. Non è molto, come vedi,
ma con un equipaggiamento più ingombrante saresti troppo
vistoso. Ora cerca di riposare, ti aspetta un lungo viaggio."
Riprende la boccetta e la infila in una sacca col resto degli
oggetti, poi esce dalla porta e la chiude alle sue spalle. Sei
molto teso, ma dopo qualche minuto ti lasci andare ad un
profondo sonno.
Il mattino seguente ti svegli alle prime luci dell'alba,
accarezzato dai primi raggi del sole. Raccogliendo il tuo
equipaggiamento, preghi gli dei affinché ti proteggano nel
corso della tua missione. Il Maestro Anziano ti aspetta
all'esterno del portone principale del tempio, tenendo per le
briglie uno stallone dal pelo chiaro. Ti aiuta a salire, e ti
indica il sentiero verso est.
"È tempo di partire. Ricorda che sei la nostra unica speranza;
agisci con prudenza, e non temere mai: gli dei vigilano su di
te e ti aiuteranno a compiere la tua missione. Abbi coraggio,64
e difendi l'onore della nostra Gilda."
Ti porge un ultimo sorriso, poi dà una sonora pacca sulla
schiena del cavallo, che parte a tutta velocità giù per la
stradina lastricata del tempio. Quando esci dal cancello
principale e ti volti indietro per salutarlo un'ultima volta, il
Maestro è scomparso.
--->55




55
Il viaggio verso la foresta dura due giorni interi. Per gran
parte del tempo non incroci anima viva; solo talvolta scorgi
sparuti gruppi di contadini al lavoro nei campi, e la mattina
del secondo giorno incontri una piccola carovana di
pellegrini diretti al tempio. Anche le prime due notti
trascorrono tranquille e senza incidenti. Il terzo giorno, poco
dopo l'alba, giungi al limitare della foresta. Dapprima il
terreno inizia a coprirsi di piccole piante e bassi arbusti, poi
man mano che avanzi gli alberi si fanno sempre più fitti,
finché la strada diventa uno stretto sentiero circondato dalla
vegetazione.
{stamina-05}
Per un paio d'ore avanzi senza problemi; poi, all'improvviso,
mentre sei assorto fra i tuoi pensieri, senti un fischio e un
rumore metallico. Quando stai per riprendere il viaggio odi
un rumore secco poco più in alto, fra i rami degli alberi alla
tua destra; fai appena in tempo a voltarti per scorgere una
grossa pietra che cade verso di te.
[abilita-1]--->127
-else->330



127
Riesci a scansarti appena in tempo per evitare la pietra, che
cade di un soffio alle tue spalle; il tuo cavallo però è stato
colpito alla schiena, e comincia a scalciare per il dolore. Da
un lato del sentiero sbuca un brigante armato di un coltello;
con un abile balzo, scendi dalla sella e ti prepari a
fronteggiare il tuo avversario.
[C:BRIGANTE:Abilità 6,Resistenza 12,Perdita 4]--->64
//Ricordati di aggiungere 1 punto alla tua Esperienza per aver
//superato la prova di Abilità. Non appena riesci a ridurre la
//sua Resistenza a 4 punti o meno, vai al 64.
-else->0



0
Sei tristemente morto in una landa desolata.




64
Sferri un colpo al torace del tuo avversario, che si piega in
due per il dolore, e lo sbatti a terra con una gomitata. Trema
di terrore, mentre gli punti l’arma alla gola.
"Sei solo?"
"Ssssì, sono solo..."
"Chi ti ha mandato?"
"Ne... nessuno, signore, nessuno! Ero qui di... di vedetta!"
"Di vedetta per cosa?"
"Ecco... quando passa qualcuno, il capo vuole che... beh..."
Gli sferri un potente calcio al bassoventre.
"Chi è il capo da queste parti?"
"Il... il capo? Beh, è Streedhal, no?"
"Avete una base da queste parti?"
Il brigante annuisce, senza staccare un solo attimo gli occhi
dall’arma che lo minaccia.
"Dov'è?"
Sulle prime si rifiuta di risponderti, scuotendo la testa; ma
con un potente schiaffo riesci a convincerlo a parlare. Alza
una mano tremolante, e indica gli alberi alla sinistra del
sentiero.
"C'è una specie di sentiero laggiù fra gli alberi. Seguilo, poi
procedi verso est. Va' pure, se vuoi, non hai speranza, là sono
in molti, e il capo..."
Lo fai tacere con un ultimo colpo alla testa. Con estrema
circospezione, ti avvicini all'albero da cui l'hai visto sbucare,
e fra le radici trovi una Sacca. Al suo interno trovi della
frutta e della carne secca (equivalenti ad una razione
alimentare), un pugnale e dei vestiti. C'è anche un piccolo
borsellino di pelle, contenente cinque monete d'argento.
Ricordati di aggiungere 1 punto alla tua Esperienza per aver
vinto il Combattimento. Puoi prendere il cibo, il pugnale e le monete, 
se lo desideri, segnandoli sul Registro d'Avventura.
[guerriero]--->201
[ladro]--->201
-else->45



201
La spiegazione del brigante ti sembra plausibile. Da anni si
dice che i ladri abbiano un importante covo da queste parti:
cercare di intrufolarsi nel nascondiglio potrebbe essere utile
per scoprire i piani del tuo avversario, ed eliminando i suoi
sgherri faresti un indubbio servizio per la comunità, ma il
Maestro Anziano ti ha raccomandato di essere prudente e
non dimenticare mai la tua missione. Inoltre, è piuttosto
verosimile che oltre quegli alberi si nasconda una trappola
ben più temibile di un solo ladro...
--->110, Se vuoi procedere in quella direzione,  
--->303, altrimenti continua lungo il percorso principale.



45
Non sai se puoi fidarti delle parole del brigante. Che ci sia
effettivamente un covo di banditi nelle vicinanze o meno,
procedere nel sottobosco sarebbe sicuramente rischioso: non
è per nulla assurdo che oltre quegli alberi si nasconda una
trappola ben più temibile del povero ladro che hai sconfitto.
Inoltre dovresti lasciare il tuo cavallo, che non riuscirebbe
mai a passare in mezzo agli arbusti.
--->12, Se decidi di rischiare e seguire la strada che ti ha indicato il ladro,
--->303, altrimenti procedi lungo il sentiero principale.



303
Il sentiero prosegue per alcune miglia fra gli alberi secolari.
Verso mezzogiorno giungi a un bivio: il sentiero principale
procede dritto, e poco più avanti piega a sinistra. Alla tua
destra invece si apre un sentiero secondario, che sale verso le
pendici della collina: è molto stretto, ma potresti ugualmente
percorrerlo a cavallo, anche se a fatica. Stai valutando come
proseguire, quando noti nel sottobosco qualcosa di strano:65
poco più avanti, alla tua sinistra, gli alberi sembrano
illuminati da una luce blu pulsante, ma non riesci a scorgerne
la fonte.
--->70, Se decidi di procedere dritto di fronte a te, lungo il sentiero principale;  
--->222, se vuoi provare a salire il sentiero di destra; 
--->157, se vuoi investigare l'origine della strana luce.



157
Ti addentri nel sottobosco in direzione della luce,
procedendo con estrema attenzione. All'improvviso, però,
qualcosa sbuca dal terreno e ti graffia al collo. Ti trovi di
fronte una esile creatura che sembra costituita da minuscoli
filamenti bluastri, simili a ramoscelli luminosi. È uno Styrax,
un rarissimo folletto dei boschi.
{stamina-02} Perdi 2 punti di Resistenza. 
--->189,Se vuoi attaccare la creatura;
--->48 se vuoi provare a comunicare in qualche modo; 
--->303 se preferisci tornare indietro al crocevia.



56
Segui il giovane per un po' a qualche metro di distanza; a un
certo punto, il ragazzo si infila in un vicolo buio, e prosegue
fino a una casa in cattivo stato. Quando si ferma sulla porta,
ti lanci in avanti e lo sbatti contro la parete di legno. Il
giovane lancia prima un urlo e poi un'imprecazione.
"Ma che diavolo combini? Cosa vuoi da me?"
"Perché te ne sei andato così di fretta dall'emporio? Nascondi
qualcosa?"
"Nascondo... qualcosa? Che dovrei nascondere?"
Gli sfili di mano il pacchetto, e scopri che dentro ci sono solo
ami ed esche da pesca. Sei un po' perplesso...
"Il vecchio si è sbagliato: mi ha dato un'esca in più. Ben gli
sta, così impara a fregare sempre la gente! Appena me ne
sono accorto me la sono battuta, altrimenti quello era capace
di chiamarmi indietro e farmela pagare. Ora, se non ti
dispiace..."
Ti toglie di mano il pacchetto ed apre la porta. Prima di
chiuderla, ti fa un gesto poco carino con una mano, e in
fondo, non ti senti di biasimarlo. Mestamente, torni verso la
locanda. Ormai il sole è definitivamente calato.
Perdi 1 punto di Mana.
{mana-01} 
--->371



330
Non riesci a evitare l'impatto con il masso, che ti colpisce in
pieno petto. Ti manca ancora il fiato, ma non hai tempo per
recuperare: un brigante è sbucato dal sentiero, sfoderando un
coltello. Balzi giù da cavallo, e ti prepari ad affrontarlo.
[C:BRIGANTE:Abilità 6, Resistenza 12,Resa 4] --->64
Perdi 3 punti di Resistenza a causa dell'impatto.
{stamina-03} 
