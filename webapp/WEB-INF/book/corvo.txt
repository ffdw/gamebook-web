IL CORVO


INTRO
Una volta in una fosca mezzanotte, mentre io meditavo, debole e stanco,
sopra alcuni bizzarri e strani volumi d'una scienza dimenticata;
mentre io chinavo la testa, quasi sonnecchiando - d'un tratto, sentii un colpo leggero,
come di qualcuno che leggermente picchiasse - pichiasse alla porta della mia camera.
« È qualche visitatore - mormorai - che batte alla porta della mia camera »
Questo soltanto, e nulla più.
--->33, Accendi il gramofono
--->34, Vado alla finestra


2
Ah! distintamente ricordo; era nel fosco Dicembre,
e ciascun tizzo moribondo proiettava il suo fantasma sul pavimento.
Febbrilmente desideravo il mattino: invano avevo tentato di trarre
dai miei libri un sollievo al dolore - al dolore per la mia perduta Eleonora,
e che nessuno chiamerà in terra - mai più.



3
E il serico triste fruscio di ciascuna cortina purpurea,
facendomi trasalire - mi riempiva di tenori fantastici, mai provati prima,
sicchè, in quell'istante, per calmare i battiti del mio cuore, io andava ripetendo:
« È qualche visitatore, che chiede supplicando d'entrare, alla porta della mia stanza.
« Qualche tardivo visitatore, che supplica d'entrare alla porta della mia stanza;
è questo soltanto, e nulla più ».



4
Subitamente la mia anima divenne forte; e non esitando più a lungo:
« Signore - dissi - o Signora, veramente io imploro il vostro perdono;
« ma il fatto è che io sonnecchiavo: e voi picchiaste sì leggermente,
« e voi sì lievemente bussaste - bussaste alla porta della mia camera,
« che io ero poco sicuro d'avervi udito ». E a questo punto, aprii intieramente la porta.
Vi era solo la tenebra, e nulla più.



5
Scrutando in quella profonda oscurità, rimasi a lungo, stupito impaurito
sospettoso, sognando sogni, che nessun mortale mai ha osato sognare;
ma il silenzio rimase intatto, e l'oscurità non diede nessun segno di vita;
e l'unica parola detta colà fu la sussurrata parola «Eleonora!»
Soltanto questo, e nulla più.



6
Ritornando nella camera, con tutta la mia anima in fiamme;
ben presto udii di nuovo battere, un poco più forte di prima.
« Certamente - dissi - certamente è qualche cosa al graticcio della mia finestra ».
Io debbo vedere, perciò, cosa sia, e esplorare questo mistero.
È certo il vento, e nulla più.



7
Quindi io spalancai l'imposta; e con molta civetteria, agitando le ali,
si avanzò un maestoso corvo dei santi giorni d'altri tempi;
egli non fece la menoma riverenza; non esitò, nè ristette un istante
ma con aria di Lord o di Lady, si appollaiò sulla porta della mia camera,
s'appollaiò, e s'installò - e nulla più.



8
Allora, quest'uccello d'ebano, inducendo la mia triste fantasia a sorridere,
con la grave e severa dignità del suo aspetto:
« Sebbene il tuo ciuffo sia tagliato e raso - io dissi - tu non sei certo un vile,
« orrido, torvo e antico corvo errante lontanto dalle spiagge della Notte
« dimmi qual'è il tuo nome signorile sulle spiagge avernali della Notte! »
Disse il corvo: « Mai più ». (1)



9
Mi meravigliai molto udendo parlare sì chiaramente questo sgraziato uccello,
sebbene la sua risposta fosse poco sensata - fosse poco a proposito;
poichè non possiamo fare a meno d'ammettere, che nessuna vivente creatura umana,
mai, finora, fu beata dalla visione d'un uccello sulla porta della sua camera,
con un nome siffatto: « Mai più ».



10
Ma il corvo, appollaiato solitario sul placido busto, profferì solamente
quest'unica parola, come se la sua anima in quest'unica parola avesse effusa.
Niente di nuovo egli pronunziò - nessuna penna egli agitò -
finchè in tono appena più forte di un murmure, io dissi: « Altri amici mi hanno già abbandonato,
domani anch'esso mi lascerà, come le mie speranze, che mi hanno già abbandonato ».
Allora, l'uccello disse: « Mai più ».



11
Trasalendo, perchè il silenzio veniva rotto da una risposta sì giusta:
« Senza dubbio - io dissi - ciò ch'egli pronunzia è tutto il suo sapere e la sua ricchezza,
« presi da qualche infelice padrone, che la spietata sciagura
« perseguì sempre più rapida, finchè le sue canzoni ebbero un solo ritornello,
« finchè i canti funebri della sua Speranza ebbero il malinconico ritornello:
« Mai, - mai più ».



12
Ma il corvo inducendo ancora tutta la mia triste anima al sorriso,
subito volsi una sedia con ricchi cuscini di fronte all'uccello, al busto e alla porta;
quindi, affondandomi nel velluto, mi misi a concatenare
fantasia a fantasia, pensando che cosa questo sinistro uccello d'altri tempi,
che cosa questo torvo sgraziato orrido scarno e sinistro uccello d'altri tempi
intendea significare gracchiando: « Mai più ».



13
Così sedevo, immerso a congetturare, senza rivolgere una sillaba
all'uccello, i cui occhi infuocati ardevano ora nell'intimo del mio petto;
io sedeva pronosticando su ciò e su altro ancora, con la testa reclinata adagio
sulla fodera di velluto del cuscino su cui la lampada guardava fissamente;
ma la cui fodera di velluto viola, che la lampada guarda fissamente
Ella non premerà, ah! - mai più!



14
Allora mi parve che l'aria si facesse più densa, profumata da un incensiere invisibile,
agiato da Serafini, i cui morbidi passi tintinnavano sul soffice pavimento,
- « Disgraziato! - esclamai - il tuo Dio per mezzo di questi angeli ti à inviato
« il sollievo - il sollievo e il nepente per le tue memorie di Eleonora!
« Tracanna, oh! tracanna questo dolce nepente, e dimentica la perduta Eleonora!
Disse il corvo: « Mai più ».



15
- « Profeta - io dissi - creatura del male! - certamente profeta, sii tu uccello o demonio! -
- « Sia che il tentatore l'abbia mandato, sia che la tempesta t'abbia gettato qui a riva,
« desolato, ma ancora indomito, su questa deserta terra incantata
« in questa visitata dall'orrore - dimmi, in verità, ti scongiuro -
« Vi è - vi è un balsamo in Galaad? dimmi, dimmi - ti scongiuro. -
Disse il corvo: « Mai più ».



16
- « Profeta! - io dissi - creatura del male! - Certamente profeta, sii tu uccello o demonio!
« Per questo Cielo che s'incurva su di noi - per questo Dio che tutti e due adoriamo -
« dì a quest'anima oppressa dal dolore, se, nel lontano Eden,
« essa abbraccerà una santa fanciulla, che gli angeli chiamano Eleonora,
« abbraccerà una rara e radiosa fanciulla che gli angeli chiamano Eleonora ».
Disse il corvo: « Mai più ».



17
- « Sia questa parola il nostro segno d'addio, uccello o demonio! » - io urlai, balzando in piedi.
« Ritorna nella tempesta e sulla riva avernale della notte!
« Non lasciare nessuna piuma nera come una traccia della menzogna che la tua anima ha profferita!
« Lascia inviolata la mia solitudine! Sgombra il busto sopra la mia porta!
Disse il corvo: « Mai più ».



18
E il corvo, non svolazzando mai, ancora si posa, ancora è posato
sul pallido busto di Pallade, sovra la porta della mia stanza,
e i suoi occhi sembrano quelli d'un demonio che sogna;
e la luce della lampada, raggiando su di lui, proietta la sua ombra sul pavimento,
e la mia, fuori di quest'ombra, che giace ondeggiando sul pavimento
non si solleverà mai più! 



19
Io appartengo a una stirpe nota per vigore di fantasia e ardore di passione. Gli uomini mi hanno chiamato pazzo; ma ancora non è risolta la questione se la pazzia sia o non sia l'intelligenza più elevata, se molto di ciò che è glorioso, se tutto ciò che è profondo, non scaturisca da una malattia del pensiero, da umori della mente esaltati a spese dell'intelletto generale. Coloro che sognano a occhi aperti avvertono molte cose che sfuggono a chi sogna soltanto di notte. Nelle loro grigie visioni essi afferrano squarci d'eternità, e svegliandosi vibrano intimamente allo scoprire di essere stati sul limitare del gran segreto. A tratti, imparano qualcosa della sapienza che riguarda il bene, e qualcosa di più sulla pura conoscenza del male. Penetrano, benché senza bussola e timone, nel vasto oceano della ' luce ineffabile ' e ancora, come gli avventurieri del geografo nubiano, "agressi sunt mare tenebrarum, quid in eo esset exploraturi".



20
Diremo allora che sono pazzo. Ammetto, almeno, che la mia esistenza mentale ha due condizioni distinte: uno stato di ragione lucida, indiscutibile, e relativa alla memoria di eventi che formano la prima epoca della mia vita, e una condizione d'ombra e di dubbio, legata al presente e al ricordo di quella che costituisce la seconda grande epoca della mia vita. Perciò quanto dirò del periodo precedente credetelo; e a quanto potrò narrare del tempo successivo date solo quel credito che vi sembri dovuto; o, se non saprete dubitarne, comportatevi come Edipo di fronte al suo enigma. La donna che amai in gioventù e della quale vergo ora calmo e preciso questi ricordi, era figlia unica della sorella di mia madre, da tempo dipartita. Eleonora era il nome di mia cugina.



21
Avevamo sempre vissuto insieme, sotto un sole tropicale, nella Valle dell'Erba Multicolore. Mai passo fortuito giunse a quella valle; poiché giaceva lontano fra una catena di alture giganti che la sovrastavano tutt'intorno, escludendo la luce del sole dai suoi più dolci recessi. Non un sentiero era battuto in vicinanza; e per raggiungere la nostra casa felice bisognava scostare con la forza il fogliame di molte migliaia di alberi della foresta, e schiacciare mortalmente le glorie di molti milioni di fiori fragranti. E così vivevamo soli, senza nulla sapere del mondo oltre la valle, io, mia cugina e sua madre.



22
Dalle regioni indistinte oltre le montagne che limitavano all'estremità superiore il nostro dominio isolato, sbucava un fiume stretto e profondo, più lucente d'ogni altra cosa tranne gli occhi di Eleonora; e serpeggiando lento in molti meandri, si allontanava infine attraverso una gola ombrosa tra alture ancor più indistinte di quelle da cui era scaturito. Noi lo chiamavamo ' Fiume del Silenzio '; poiché nel suo fluire pareva ìnsito un alone taciturno. Non un mormorìo sorgeva dal suo letto, e così dolcemente errava seguendo il suo corso che i ciottoli perlacei cari al nostro sguardo, giù in fondo al suo seno, non si muovevano per nulla, ma giacevano in immobile contentezza, ciascuno al suo vecchio posto, brillando gloriosamente perenni.



23
Il margine del fiume, e dei molti abbaglianti ruscelli che per vie oblique vi confluivano, come pure gli spazi che dai margini si stendevano alle profondità dei corsi d'acqua sino a raggiungerne il letto sassoso, questi spiazzi, non meno della valle in tutta la sua superficie dal fiume alle montagne circostanti, erano tappezzati di un'erbetta tenera, verde, fitta, perfettamente pareggiata e profumata di vaniglia, ma talmente costellata di gialli ranuncoli, candide margherite, violette purpuree e asfodeli rossi come rubini, che la sua generosa bellezza parlava ad alta voce ai nostri cuori dell'amore e della gloria di Dio. E qua e là, a boschi sparsi per quest'erba come intrichi di sogni, sorgevano alberi fantastici, i cui tronchi slanciati non erano diritti ma s'inclinavano graziosamente verso la luce affacciantesi a mezzogiorno sul centro della valle. La loro corteccia si variegava d'uno splendore alterno d'ebano e argento, ed era più liscia d'ogni cosa tranne le guance d'Eleonora; cosicché se non fosse stato per il verde brillante delle enormi foglie che dalle cime si spandevano a lunghe linee tremule, scherzando con gli zefiri, li si sarebbe potuti scambiare per giganteschi serpenti di Siria che rendessero omaggio al loro Sovrano, il Sole.



24
La mano nella mano, per questa valle ben quindici anni vagai con Eleonora prima che Amore entrasse nei nostri cuori. Fu una sera al volgere del terzo lustro della sua vita, e quarto della mia, che ci sedemmo stretti in reciproco abbraccio sotto gli alberi serpentini, e abbassando lo sguardo sul Fiume del Silenzio vi cercammo, nel vivo dell'acqua, le nostre immagini. Per il resto di quella dolce giornata non dicemmo una parola; e anche all'indomani le nostre parole furono tremule e rade. Avevamo tratto da quell'onda il dio Eros, e ora sentivamo che egli ci aveva acceso dentro le anime di fuoco degli antenati. Le passioni che da secoli contraddistinguevano la nostra stirpe affiorarono in folla con gli impeti visionari per cui andava altrettanto famosa, e assieme spirarono una delirante felicità sulla Valle dell'Erba Multicolore. Colse ogni cosa un mutamento. Strani fiori brillanti a forma di stella scoppiarono sugli alberi dove non s'era mai vista traccia di fiore. Le tinte del tappeto verde si fecero più intense; e quando ad una ad una appassirono le bianche margherite, sbocciarono al loro posto a dieci per volta gli asfodeli color del rubino. E la vita trionfava sul nostro cammino; poiché l'alto fenicottero, sinora invisibile, con tutti gli altri uccelli allietati da fulgido piumaggio sfoggiava davanti a noi le sue ali scarlatte. Pesci d'oro e d'argento frequentavano il fiume, dal cui seno esalava a poco a poco un mormorìo crescente fino a farsi soave melodìa più divina dell'arpa eòlia; più dolce d'ogni altra voce tranne quella d'Eleonora. E ora pure una nube voluminosa, che avevamo a lungo osservato nelle regioni di Espero, ne salpò, in uno sfarzo di crèmisi e d'oro, e fermatasi in pace sopra di noi affondò di giorno in giorno sempre di più, finché non giunse a poggiare con gli orli sulle vette dei monti, convertendone la penombra in splendore e rinserrandoci sempre in una magica prigione di grandiosità e di gloria. La leggiadrìa di Eleonora era quella dei serafini; ma la fanciulla era ignara e innocente come la sua breve vita trascorsa tra i fiori. Nessuna astuzia mascherava il fervido amore che le avvivava il cuore, e con me essa esaminò i suoi più intimi recessi mentre insieme passeggiavamo per la Valle dell'Erba Multicolore, e discorrevamo dei grandi cambiamenti che vi si erano prodotti.



25
Finalmente, avendo parlato un giorno, tutta in lacrime, del mutamento estremo che doveva incogliere all'Umanità, da allora in poi si soffermò unicamente su questo tema doloroso, intessendolo in tutto il nostro conversare, come nelle canzoni del bardo Sciraz si vedono ricorrere più volte le stesse immagini in ogni espressiva variazione di fraseggio.



26
Aveva visto che il dito della Morte era sul suo petto, che al pari della effimera essa era stata fatta in perfezione di forme solo per morire; ma i terrori della tomba, per lei, stavano soltanto in una considerazione che mi rivelò, in un crepuscolo vespertino, presso le rive del Fiume del Silenzio. La addolorava il pensiero che io, dopo averla inumata nella Valle dell'Erba Multicolore, ne abbandonassi per sempre i recessi felici, per donare a qualche fanciulla del mondo esterno e quotidiano l'amore che adesso era così appassionatamente suo. E io subito mi gettai ai piedi di Eleonora, e feci voto a lei e al Cielo di non legarmi mai in matrimonio a nessuna figlia della Terra, di non venir mai meno alla sua cara memoria, o alla memoria del devoto affetto che mi aveva elargito. E invocai il Re Sovrano dell'Universo a testimone della pia solennità del mio voto. E la maledizione che da Lui e da lei, santa d'Helusion, invocai, qualora tradissi quella promessa, comportava un castigo di tale immenso orrore che non posso qui precisarlo. E gli occhi luminosi di Eleonora si fecero più luminosi alle mie parole; e sospirò come se un peso mortale le fosse stato levato dal petto; e tremò e amaramente pianse; ma accettò il voto (era forse altro che una bambina?) ed esso le rese lieve il morire. E dal letto della sua morte tranquilla mi disse di lì a non molti giorni, che a causa di quanto avevo fatto per confortare il suo spirito, in quello spirito avrebbe vegliato su di me dopo la dipartita, e se le era concesso sarebbe visibilmente tornata a me nelle veglie notturne; ma che se questa cosa non era in potere delle anime del Paradiso, mi avrebbe almeno dato frequenti indizi della sua presenza; sospirando su di me nei venti della sera, o riempiendo l'aria che respiravo di profumi esalati dagli incensieri degli angeli. E con queste parole sulle labbra rese a Dio la sua vita innocente, ponendo fine alla prima epoca della mia vita.



27
Finora ho parlato in modo veritiero. Ma varcando la barriera che la morte della mia amata forma nel sentiero del Tempo, e passando alla seconda epoca della mia esistenza, sento un'ombra addensarmisi sul cervello e diffido della lucidità o attendibilità dei miei ricordi. Ma proseguiamo. Gli anni si trascinavano pesanti, e ancora dimoravo nella Valle dell'Erba Multicolore; ma un secondo mutamento era sopraggiunto in tutte le cose. I fiori a forma di stella si ritrassero nei tronchi degli alberi, e non ricomparvero più. Le tinte del tappeto verde svanirono; e ad uno ad uno gli asfodeli color del rubino avvizzirono; e al loro posto spuntarono scure viole simili ad occhi, che si torcevano inquiete sotto un gravame perpetuo di rugiada. E la Vita si allontanò dai nostri sentieri; poiché l'alto fenicottero non sfoggiò più davanti a noi il suo piumaggio scarlatto, ma triste svolò dalla valle alle colline, con tutti i fulvidi uccelli che in sua compagnia erano giunti ad allietarci. E i pesci d'oro e d'argento guizzarono fuori dalla gola che delimitava il nostro dominio dalla parte più bassa e non animarono più il dolce fiume. E la soave melodia più delicata dell'arpa eòlia mossa dal vento e più divina d'ogni altra voce tranne quella d'Eleonora, morì a poco a poco, facendosi sempre più sommessa nel suo mormorio, finché il fiume non risprofondò nella solennità del suo silenzio originario. E poi, da ultimo la nube voluminosa si alzò, e abbandonando le cime dei monti all'antica penombra ricadde nelle regioni di Espero, e privò di tutta la sua gloria d'oro la Valle dell'Erba Multicolore.



28
Eppure le promesse di Eleonora non furono dimenticate; poiché udivo il suono degli oscillanti incensieri degli angeli; fiumane di sacro profumo aleggiavano perenni sulla valle; e nelle ore solitarie, quando il cuore mi batteva più greve, i venti giungevano alla mia fronte carichi di tenui sospiri; e mormorii indistinti spesso riempivano l'aria notturna; e una volta - oh, ma solo una volta! - mi destò da un sonno come di morte il bacio di labbra spirituali.



29
Ma anche così il vuoto del mio cuore non si colmava. Anelavo all'amore che un tempo l'aveva riempito fino a traboccarne. Alla fine la valle mi riuscì penosa per il ricordo di Eleonora, e la lasciai per sempre per le vanità e i turbolenti trionfi del mondo.



30
Mi trovavo in una città straniera, dove tutto poteva giovare a cancellarmi dal ricordo i dolci sogni così a lungo sognati nella Valle dell'Erba Multicolore. Fasto e cerimonie d'una corte maestosa, e il folle strepito delle armi, e la raggiante bellezza delle donne, mi stordivano d'ebbrezza. Ma finora l'anima mia si era mantenuta fedele ai suoi voti, e ancora mi giungevano nelle silenziose ore notturne gli indizi della presenza di Eleonora. Di colpo queste manifestazioni cessarono; e il mondo si oscurò davanti ai miei occhi; e rimasi allibito ai pensieri brucianti che mi possedevano, alle terribili tentazioni che mi insidiavano; poiché da qualche terra lontana, lontana e sconosciuta, venne alla gaia corte del re che servivo una fanciulla alla cui bellezza tutto il mio cuore infedele subito cedette; ai suoi piedi mi chinai senza lotta, nella più ardente, nella più schiava adorazione d'amore. E che cos'era infatti la mia passione per la giovinetta della valle in confronto al fervore, al delirio e all'esaltante estasi di adorazione con cui riversavo in lacrime tutta l'anima mia ai piedi dell'etèrea Ermengarda? Oh, luminosa era la serafica Ermengarda! e in questa certezza non avevo posto per altra. Oh, divina era l'angelica Ermengarda! e guardando nelle profondità dei suoi occhi memori pensavo soltanto ad essi; e a lei.



31
Mi sposai; e non temetti la maledizione che avevo invocato; e la sua amarezza non mi fu inflitta. E una volta, solo una volta ancora nel silenzio della notte, mi giunsero attraverso le persiane i tenui sospiri che mi avevano abbandonato; e si modellarono in una voce soave e familiare, che diceva:



32
"Dormi in pace! perché lo Spirito d'Amore regna sovrano, e stringendo al tuo cuore appassionato colei che è Ermengarda, tu sei sciolto, per ragioni che ti saranno rese note in Cielo, dai tuoi voti verso Eleonora".



33
Per coprire il rumore esterno decido di alzarmi e mettere un disco sul mio gramofono. Poggio la testina sul disco e mi abbandono alle note. 
--->35, Torno alla scrivania



34
Incuriosito dallo strano picchiettare vado alla finestra per scrutare attravero la tenda e carpire all'oscurità la figura di cosa si muove all'esterno.



35
Torno alla scrivania e mi rimetto leggere le carte, la concentrazione mi trascina nel mare dei miei pensieri da studioso.